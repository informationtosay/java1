public class Calculator1
{
 
public static void main(String[] args)
{
 Calculator1 cc = new Calculator1();
 //int res = cc.add();
 System.out.println("main => "+cc.add());
 cc.divide(10);//parameter//5
 cc.divide(100,5);
 cc.divide(20);
 //cc.divide("hello");  
}
 //method overloading -> compile time polymorphism
 //method overLoading - same method name with diff type of argumnets
public void divide(String str)
{
 System.out.println(str);
}
 //method overLoading - same method name with diff no of arguments
public void divide(int no)//argument int no = "hello";
{
 //int res = no/2;
 System.out.println(no/2);
}
 public void divide(int no,int no2)
{
 System.out.println(no/no2);
}
 //named set of instructions
public int add()
{
 //int no1=10;
 //int no2=20;
 //int no3=no1+no2;
 //System.out.println("add => "+no3);
 return 10+20;
}

}
