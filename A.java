public class A
{
	//static refers to class specific information
static String housename="Green House";
static int doorno=15;
	//non static refers to object specific information
String name="Ramu";
int age=29;
	
public static void main(String[] args)
{
	//local variable
int bookprice=480;
	A Obj=new A();
	
	System.out.println(bookprice);
	System.out.println(A.doorno);
	System.out.println(A.housename);
	System.out.println(Obj.name);
}
}
