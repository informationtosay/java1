public class Read
{
 //static refers to class specific information
 static String name = "Adithya";
 static int age = 15;
 //Non-static variable or instance variable
 String name2="abc";
 int age2;
 boolean stud;
 float weight;
 
 //byte short int long -> 0
 //double float -> 0.0
 //boolean - false
 //string - null
 //char - ''

public static void main(String[] args)
{ //local variable -> method , block , constructor
 int no = 45;
 Read.name="dgfdsgsfdg";
 //System.out.println(no);
 
 //System.out.println(Read.age);

 Read person1 = new Read();
 person1.name2="hari";
 person1.age2=20;
 
	 Read.name="stepan";

 Read person2 = new Read();
 person2.name2="ramu";
 person2.age2=28;
 
 Read person3 = new Read();
 
 System.out.println(Read.name); 
 System.out.println(name);
 System.out.println(person1.name); 
 System.out.println(person3.name);
 
 
 System.out.println(person1.name2);//hari
 System.out.println(person1.age2);
 System.out.println(person2.name2);//ramu
 System.out.println(person2.age2);
 
 System.out.println(person3.name2);//abc
 System.out.println(person3.age2);
 System.out.println(person3.stud);
 System.out.println(person3.weight);
 
 //System.out.println(age3);
 
 System.out.println(person1.name);
 System.out.println(person2.name);
 
 
}
 
 
}
